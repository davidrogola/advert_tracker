package main

import (
	"advert_tracker/http/transport"
	"advert_tracker/models"
	"advert_tracker/services"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	kitlogger "github.com/go-kit/kit/log"
	"go.elastic.co/apm/module/apmhttp"

	"advert_tracker/database"
	// Add mysql's database driver, used to connect to the database
	"github.com/go-kit/kit/log/level"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func main() {
	const (
		httpPort         = "8050"
		advertTrackerEnv = "ADVERT_TRACKER_ENV"
	)
	var logger = services.InitLogger(os.Stdout, level.AllowInfo(), true)

	env, found := os.LookupEnv(advertTrackerEnv)
	if !found {
		strErr := "Could not determine the environment, check whether you had set env variable " + advertTrackerEnv
		level.Error(logger).Log("env_error", strErr)
		panic(strErr)
	}
	// Initialize application configurations(connection strings and endpoints)
	config, err := services.InitConfig()
	if err != nil {
		strErr := fmt.Sprintf("An error '%s' occured while initializing configs", err)
		level.Error(logger).Log("config_err", strErr)
		panic(err)
	}
	// Database set up
	conn, err := config.GetDBConnStr(env)
	level.Info(logger).Log("conn_str", conn)
	if err != nil {
		level.Error(logger).Log("conn_str_err", err)
		panic(err)
	}
	var db *gorm.DB
	db, err = initDB(conn, logger)
	if err != nil {
		level.Error(logger).Log("dbconn_err", fmt.Sprintf("An error '%s' occured while connecting to the database", err))
		// Service should not start if a succesful database connection is not established so we exit
		panic(err)
	}
	//Init repositories
	clientRepo := database.NewClientRepository(db)
	agencyRepo := database.NewAgencyRepository(db)
	defer db.Close()

	// Init services
	clientService := services.NewClientService(clientRepo, logger)
	agencyService := services.NewAgencyService(agencyRepo, logger)
	//Initialize the http server and register routes
	mux := http.NewServeMux()
	mux.Handle("/", transport.MakeClientHandler(clientService, logger))
	mux.Handle("/", transport.MakeAgencyHTTPHandler(agencyService, logger))

	errs := make(chan error, 2)
	go func() {
		var httpAddr = flag.String("http", ":"+httpPort, "HTTP listen address")
		flag.Parse()
		level.Info(logger).Log("server_online", fmt.Sprintf("Advert tracker service is running on %s environment & listening on address http://localhost:8050", env))
		errs <- http.ListenAndServe(*httpAddr, apmhttp.Wrap(mux))
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	level.Error(logger).Log("server_terminated", <-errs)

}

func initDB(conn string, logger kitlogger.Logger) (*gorm.DB, error) {
	db, err := gorm.Open("mysql", conn)
	if err != nil {
		//Connection to DB should be established, otherwise service will abort
		level.Error(logger).Log("connection_str", conn, "connection_error", err)
		return nil, err
	}

	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&models.ClientType{}, &models.AdvertType{}, &models.Client{}, &models.Advert{},
		&models.Agency{}, &models.AgencyClient{}, &models.AdvertVerification{}, &models.AdvertVerificationImage{})
	level.Info(logger).Log("db_conn", "Connection to the database successful")
	return db, nil
}
