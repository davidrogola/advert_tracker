module advert_tracker

go 1.12

require (
	github.com/creack/pty v1.1.9 // indirect
	github.com/elastic/go-sysinfo v1.1.0 // indirect
	github.com/elastic/go-windows v1.0.1 // indirect
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/hashicorp/consul/api v1.2.0 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/kr/pty v1.1.8 // indirect
	github.com/prometheus/client_golang v0.9.3-0.20190127221311-3c4408c8b829
	github.com/prometheus/procfs v0.0.5 // indirect
	github.com/shopspring/decimal v0.0.0-20190905144223-a36b5d85f337
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	go.elastic.co/apm/module/apmgorilla v1.5.0
	go.elastic.co/apm/module/apmhttp v1.5.0
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20190926180325-855e68c8590b // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
