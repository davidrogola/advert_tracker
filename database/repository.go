package database

import (
	"advert_tracker/models"

	"github.com/jinzhu/gorm"
)

// ClientRepository ..
type ClientRepository struct {
	db *gorm.DB
}

// NewClientRepository returns new client repository
func NewClientRepository(db *gorm.DB) *ClientRepository {
	return &ClientRepository{db: db}
}

// Add creates new client
func (clr *ClientRepository) Add(client *models.Client) (*models.Client, error) {
	err := clr.db.Create(client).Scan(client).Error
	if err != nil {
		return nil, err
	}
	return client, nil
}

// Find gets a client by id
func (clr *ClientRepository) Find(ID int, client *models.Client, preload bool) (interface{}, error) {
	err := clr.db.Set("gorm:auto_preload", preload).First(client, ID).Error
	if err != nil {
		return nil, err
	}
	return client, nil
}

// ExecuteQuery ..
func (clr *ClientRepository) ExecuteQuery(sqlQuery string, client *models.Client, values ...interface{}) (interface{}, error) {
	err := clr.db.Exec(sqlQuery, values).Scan(&client).Error
	if err != nil {
		return nil, err
	}
	return client, nil
}

// GetAll ...
func (clr *ClientRepository) GetAll(clients []*models.Client, preload bool) ([]*models.Client, error) {
	err := clr.db.Find(&clients).Error
	if err != nil {
		return nil, err
	}
	return clients, nil
}
