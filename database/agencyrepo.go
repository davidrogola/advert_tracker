package database

import (
	"advert_tracker/models"

	"github.com/jinzhu/gorm"
)

// AgencyRepository holds the db functions for agency
type AgencyRepository struct {
	db *gorm.DB
}

// NewAgencyRepository returns a new instance of the agency repo
func NewAgencyRepository(db *gorm.DB) *AgencyRepository {
	return &AgencyRepository{db: db}
}

// Add creates a new agency record
func (repo *AgencyRepository) Add(agency *models.Agency) (int, error) {
	err := repo.db.Create(&agency).Scan(agency).Error
	if err != nil {
		return 0, err
	}
	return agency.ID, nil
}

// Find gets an agency by Id
func (repo *AgencyRepository) Find(ID int, agency *models.Agency, preload bool) (*models.Agency, error) {
	err := repo.db.Set("gorm:auto_preload", preload).First(agency, ID).Error
	if err != nil {
		return nil, err
	}
	return agency, nil
}

// ExecuteQuery ..
func (repo *AgencyRepository) ExecuteQuery(sqlQuery string, agency *models.Agency, values ...interface{}) (interface{}, error) {
	err := repo.db.Exec(sqlQuery, values).Scan(&agency).Error
	if err != nil {
		return nil, err
	}
	return agency, nil
}

// LinkClient links an agency to a client
func (repo *AgencyRepository) LinkClient(agencyClient *models.AgencyClient) (bool, error) {
	err := repo.db.Create(&agencyClient).Error
	if err != nil {
		return false, err
	}
	return true, nil
}

// GetAll ...
func (repo *AgencyRepository) GetAll(agency []*models.Agency, preload bool) ([]*models.Agency, error) {
	err := repo.db.Find(&agency).Error
	if err != nil {
		return nil, err
	}
	return agency, nil
}
