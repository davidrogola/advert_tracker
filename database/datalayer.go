package database

import (
	"advert_tracker/models"

	"github.com/jinzhu/gorm"
)

// Datalayer ...
type Datalayer struct {
	db *gorm.DB
}

// Init datalayer instance
func Init(db *gorm.DB) *Datalayer {
	return &Datalayer{db: db}
}

// ExecuteQuery ..
func (datalayer *Datalayer) ExecuteQuery(sqlQuery string, result interface{}, values ...interface{}) (interface{}, error) {
	err := datalayer.db.Exec(sqlQuery, values).Scan(&result).Error
	if err != nil {
		return nil, err
	}
	return result, nil
}

// Add ...
func (datalayer *Datalayer) Add(value interface{}) (interface{}, error) {
	err := datalayer.db.Create(value).Scan(value).Error
	if err != nil {
		return nil, err
	}
	return value, nil
}

// GetByID ...
func (datalayer *Datalayer) GetByID(id int, obj interface{}, preload bool) (interface{}, error) {
	err := datalayer.db.Set("gorm:auto_preload", preload).First(obj, id).Error
	if err != nil {
		return nil, err
	}
	return obj, nil
}

// GetAll ...
func (datalayer *Datalayer) GetAll(clients []*models.Client, preload bool) ([]*models.Client, error) {
	err := datalayer.db.Find(&clients).Error
	if err != nil {
		return nil, err
	}
	return clients, nil
}
