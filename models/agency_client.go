package models

import "time"

// AgencyClient ...
type AgencyClient struct {
	ID              int
	ClientID        int       `gorm:"not null"`
	AgencyID        int       `gorm:"not null"`
	CreatedAt       time.Time `gorm:"not null"`
	CreatedBy       string    `gorm:"not null"`
	DateDeactivated time.Time
	DeactivatedBy   string
	Status          bool
}
