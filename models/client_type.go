package models

import "time"

// ClientType ...
type ClientType struct {
	ID        int
	Name      string `gorm:"size:255"`
	CreatedAt time.Time
}
