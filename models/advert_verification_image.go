package models

import "time"

// AdvertVerificationImage is the submitted image as verifaction for a given advert
type AdvertVerificationImage struct {
	ID                   int
	AdvertVerificationID int    `gorm:"not null"`
	Name                 string `gorm:"not null"`
	Path                 string
	Content              []byte
	CreatedAt            time.Time
}
