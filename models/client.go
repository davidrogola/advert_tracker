package models

import (
	"time"
)

// Client ..
type Client struct {
	ID              int
	Name            string     `gorm:"not null"`
	ClientType      ClientType `gorm:"foreignkey:ClientTypeID"`
	ClientTypeID    int        `gorm:"not null"`
	CreatedAt       time.Time  `gorm:"not null"`
	PhoneNumber     string     `gorm:"not null"`
	EmailAddress    string     `gorm:"not null"`
	Address         string     `gorm:"not null"`
	ContactPerson   string     `gorm:"not null"`
	IsActive        bool
	DateDeactivated time.Time
	DeactivatedBy   string
}
