package models

import (
	"time"
)

// Advert ...
type Advert struct {
	ID                  int
	AdvertTypeID        int                  `gorm:"not null"`
	ClientID            int                  `gorm:"not null"`
	AgencyID            int                  `gorm:"not null"`
	Duration            int                  `gorm:"not null"`
	CreatedAt           time.Time            `gorm:"not null"`
	CreatedBy           string               `gorm:"not null"`
	StartDate           time.Time            `gorm:"not null"`
	EndDate             time.Time            `gorm:"not null"`
	Longitude           float64              `sql:"DECIMAL(11,8) null"`
	Latitude            float64              `sql:"DECIMAL(10,8) null"`
	AdvertVerifications []AdvertVerification `gorm:"foreignkey:AdvertId"`
}
