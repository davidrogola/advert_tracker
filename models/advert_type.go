package models

import "time"

// AdvertType ...
type AdvertType struct {
	ID        string
	Name      string `gorm:"not null"`
	CreatedAt time.Time
}
