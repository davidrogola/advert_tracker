package models

import (
	"database/sql"
	"time"
)

// AdvertVerification contains verification details of a given advert.
type AdvertVerification struct {
	ID                 int
	AdvertID           int       `gorm:"not null"`
	CreatedAt          time.Time `gorm:"not null"`
	SubmittedBy        string    `gorm:"not null"`
	Town               string    `gorm:"not null"`
	Street             string
	LandMark           string
	Building           string
	AdditionalInfo     sql.NullString     `gorm:"size:1000"`
	Longitude          float64            `sql:"DECIMAL(11,8) null"`
	Latitude           float64            `sql:"DECIMAL(10,8) null"`
	VerificationStatus VerificationStatus `gorm:"not null"`
	VerifiedBy         string
	VerificationDate   time.Time
	VerificationNotes  sql.NullString `gorm:"size:1000"`
}

// VerificationStatus represents current status of the advert .ie pending, verified, failed, cancelled
type VerificationStatus int

const (
	// Pending verification
	Pending VerificationStatus = iota + 1
	// Verified represents a verified advert
	Verified
	// Failed verification
	Failed
	// Cancelled verification
	Cancelled
)
