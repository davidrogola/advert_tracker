package models

import (
	"time"
)

// Agency ...
type Agency struct {
	ID              int
	Name            string    `gorm:"not null"`
	CreatedAt       time.Time `gorm:"not null"`
	Address         string    `gorm:"not null"`
	Phonenumber     string    `gorm:"not null"`
	EmailAddress    string    `gorm:"not null"`
	ContactPerson   string    `gorm:"not null"`
	DateDeactivated time.Time
	DeactivatedBy   string
	IsActive        bool
	AgencyClients   []AgencyClient `gorm:"foreignkey:AgencyId"`
}
