package services

import (
	"advert_tracker/database"
	"advert_tracker/models"
	"fmt"
	"time"

	"github.com/go-kit/kit/log/level"

	"github.com/go-kit/kit/log"
)

// ClientInfo ...
type ClientInfo struct {
	ID            int       `json:"client_id"`
	Name          string    `json:"name"`
	ClientType    string    `json:"client_type"`
	PhoneNumber   string    `json:"phone_number"`
	Email         string    `json:"email"`
	Address       string    `json:"address"`
	ContactPerson string    `json:"contact_person"`
	CreatedAt     time.Time `json:"created_at"`
}

// ClientService handles the logic dealing with a client for an agency
type ClientService interface {
	AddClient(clientTypeID int, name, phoneNumber, email, contactPerson, address string) (int, error)
	GetClientByID(ID int) (*ClientInfo, error)
	GetAllClients() ([]*ClientInfo, error)
}

type service struct {
	repository *database.ClientRepository
	logger     log.Logger
}

// NewClientService creates a new instance of the client service
func NewClientService(repository *database.ClientRepository, logger log.Logger) ClientService {
	return &service{repository: repository, logger: logger}
}

func (svc *service) AddClient(clientTypeID int, name, phoneNumber, email, contactPerson, address string) (int, error) {
	var newClient = models.Client{
		Name:          name,
		ClientTypeID:  clientTypeID,
		PhoneNumber:   phoneNumber,
		EmailAddress:  email,
		Address:       address,
		ContactPerson: contactPerson,
		IsActive:      true,
	}
	client, err := svc.repository.Add(&newClient)
	if err != nil {
		level.Error(svc.logger).Log("add_client_err", fmt.Sprintf("An error '%s' occured while creating new client", err))
		return 0, err
	}
	return client.ID, nil
}

func (svc *service) GetClientByID(ID int) (*ClientInfo, error) {
	cl, err := svc.repository.Find(ID, &models.Client{}, true)
	if err != nil {
		level.Error(svc.logger).Log("get_client_err", fmt.Sprintf("An error '%s' occured while fetching client with id: %d ", err, ID))
		return nil, err
	}
	if cl == nil {
		return nil, nil
	}
	client := cl.(*models.Client)
	return &ClientInfo{
		Name:          client.Name,
		ClientType:    client.ClientType.Name,
		ContactPerson: client.ContactPerson,
		CreatedAt:     client.CreatedAt,
		PhoneNumber:   client.PhoneNumber,
		Email:         client.EmailAddress,
		Address:       client.Address,
	}, nil
}

func (svc *service) GetAllClients() ([]*ClientInfo, error) {
	resp, err := svc.repository.GetAll([]*models.Client{}, true)
	if err != nil {
		return nil, err
	}
	clients := make([]*ClientInfo, 0)
	for _, client := range resp {
		clients = append(clients, &ClientInfo{
			ID:            client.ID,
			Name:          client.Name,
			ClientType:    client.ClientType.Name,
			ContactPerson: client.ContactPerson,
			CreatedAt:     client.CreatedAt,
			PhoneNumber:   client.PhoneNumber,
			Email:         client.EmailAddress,
			Address:       client.Address,
		})
	}
	return clients, nil
}
