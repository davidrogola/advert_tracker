package services

import (
	"io"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

// InitLogger ...
func InitLogger(w io.Writer, l level.Option, ts bool) log.Logger {
	var logger log.Logger
	logger = log.NewLogfmtLogger(w)
	logger = level.NewFilter(logger, l)
	if ts {
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	}
	return logger
}
