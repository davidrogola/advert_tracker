package services

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
)

// Configuration ...
type Configuration struct {
	DbConn ConnectionString
}

// ConnectionString object holds config values for our database
type ConnectionString struct {
	Host     string
	Username string
	Password string
	Port     int
	Database string
}

const (
	// DbPort is the database port  configured as an enviroment variable
	DbPort = "DB_PORT"
	// DbUser is the database user name  configured as an enviroment variable
	DbUser = "DB_USER"
	// DbHost is the database server configured as an enviroment variable
	DbHost = "DB_HOST"
	// DbPassword is the database password configured as an enviroment
	DbPassword = "DB_PASSWORD"
	// DbName is the database name configured as an environment
	DbName = "DB_NAME"
)

// InitConfig returns a new instance of the Configuration object
func InitConfig() (*Configuration, error) {
	conf, err := loadConfigFile()
	if err != nil {
		return nil, err
	}
	return &Configuration{
		DbConn: conf.DbConn,
	}, nil
}

// GetDBConnStr returns the db connection string.
func (config *Configuration) GetDBConnStr(env string) (string, error) {
	if env == "local" {
		//Localhost env
		return buildDBConnStr(config.DbConn), nil
	}
	// Staging and production env connection strings
	port, err := strconv.Atoi(os.Getenv(DbPort))
	if err != nil {
		return "", nil
	}
	var conn = ConnectionString{Host: os.Getenv(DbHost),
		Username: os.Getenv(DbUser),
		Password: os.Getenv(DbPassword),
		Port:     port,
		Database: os.Getenv(DbName)}
	return buildDBConnStr(conn), nil
}

func buildDBConnStr(conn ConnectionString) string {
	// user:password@tcp(localhost:3306)/dbname?tls=skip-verify&autocommit=tru
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True",
		conn.Username, conn.Password, conn.Host, conn.Port, conn.Database)
}

//loadConfigFile fetches the json config file stored locally and decodes it into the Configuration object
func loadConfigFile() (*Configuration, error) {
	var config *Configuration
	file, err := os.Open("config.json")
	if err != nil {
		return nil, err
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		return config, err
	}
	return config, nil
}
