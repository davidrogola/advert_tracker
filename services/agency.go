package services

import (
	"advert_tracker/database"
	"advert_tracker/models"
	"fmt"
	"time"

	"github.com/go-kit/kit/log/level"

	"github.com/go-kit/kit/log"
)

// AgencyInfo DTO
type AgencyInfo struct {
	ID            int       `json:"client_id"`
	Name          string    `json:"name"`
	PhoneNumber   string    `json:"phone_number"`
	Email         string    `json:"email"`
	Address       string    `json:"address"`
	ContactPerson string    `json:"contact_person"`
	CreatedAt     time.Time `json:"created_at"`
}

// AgencyService service interface
type AgencyService interface {
	Add(name, phoneNumber, email, contactPerson, address string) (int, error)
	LinkClient(agencyID, clientID int) (bool, error)
	GetByID(ID int) (*AgencyInfo, error)
	GetAll() ([]*AgencyInfo, error)
}

type agencyService struct {
	repo   *database.AgencyRepository
	logger log.Logger
}

// NewAgencyService returns a new instace of agency service
func NewAgencyService(repo *database.AgencyRepository, logger log.Logger) AgencyService {
	return &agencyService{repo: repo, logger: logger}
}

func (agencySvc *agencyService) Add(name, phoneNumber, email, contactPerson, address string) (int, error) {
	agency := &models.Agency{
		Name:          name,
		Phonenumber:   phoneNumber,
		EmailAddress:  email,
		ContactPerson: contactPerson,
		Address:       address,
		IsActive:      true,
	}
	id, err := agencySvc.repo.Add(agency)
	if err != nil {
		level.Error(agencySvc.logger).Log("add_agency_err", fmt.Sprintf("An error '%s' occured while creating agency record", err))
		return 0, err
	}
	return id, nil
}

func (agencySvc *agencyService) LinkClient(agencyID, clientID int) (bool, error) {
	agencyClient := &models.AgencyClient{
		AgencyID: agencyID,
		ClientID: clientID,
	}
	linked, err := agencySvc.repo.LinkClient(agencyClient)
	if err != nil {
		level.Error(agencySvc.logger).Log("link_client_err", fmt.Sprintf("An error occured while linking agencyID %d to clientID %d", agencyID, clientID))
	}
	return linked, err
}

func (agencySvc *agencyService) GetByID(ID int) (*AgencyInfo, error) {
	agency, err := agencySvc.repo.Find(ID, &models.Agency{}, false)
	if err != nil {
		return nil, err
	}
	agencyInfo := &AgencyInfo{
		ID:            agency.ID,
		Name:          agency.Name,
		PhoneNumber:   agency.Phonenumber,
		Email:         agency.EmailAddress,
		Address:       agency.Address,
		ContactPerson: agency.ContactPerson,
		CreatedAt:     agency.CreatedAt,
	}
	return agencyInfo, nil
}

func (agencySvc *agencyService) GetAll() ([]*AgencyInfo, error) {
	agencies, err := agencySvc.repo.GetAll([]*models.Agency{}, false)
	if err != nil {
		return nil, err
	}
	agencyInfoArr := make([]*AgencyInfo, 0)
	for _, agency := range agencies {
		agencyInfoArr = append(agencyInfoArr, &AgencyInfo{
			ID:            agency.ID,
			Name:          agency.Name,
			PhoneNumber:   agency.Phonenumber,
			Email:         agency.EmailAddress,
			Address:       agency.Address,
			ContactPerson: agency.ContactPerson,
			CreatedAt:     agency.CreatedAt,
		})
	}
	return agencyInfoArr, nil
}
