package transport

import (
	"advert_tracker/http/endpoints"
	"advert_tracker/services"
	"context"
	"encoding/json"
	"net/http"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

// MakeClientHandler ...
func MakeClientHandler(service services.ClientService, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
	}

	addClientHandler := kithttp.NewServer(
		endpoints.MakeAddClientEndpoint(service),
		decodeAddClientRequest,
		encodeResponse,
		opts...,
	)
	getClientHandler := kithttp.NewServer(
		endpoints.MakeGetClientByIDEndpoint(service),
		decodeGetClientRequest,
		encodeResponse,
		opts...,
	)

	getAllClientsHandler := kithttp.NewServer(
		endpoints.MakeGetAllClientsEndpoint(service),
		decodeGetAllClientRequest,
		encodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle("/client/add", addClientHandler).Methods("POST")
	r.Handle("/client/get_by_id", getClientHandler).Methods("POST")
	r.Handle("/client/get_all", getAllClientsHandler).Methods("POST")

	return r

}

func decodeAddClientRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var rq endpoints.AddClientRequest
	if err := json.NewDecoder(r.Body).Decode(&rq); err != nil {
		return nil, err
	}
	return rq, nil
}

func decodeGetClientRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var rq endpoints.GetClientRequest
	if err := json.NewDecoder(r.Body).Decode(&rq); err != nil {
		return nil, err
	}
	return rq, nil
}

func decodeGetAllClientRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var rq endpoints.GetAllClientsRequest
	if err := json.NewDecoder(r.Body).Decode(&rq); err != nil {
		return nil, err
	}
	return rq, nil
}

// Encodes reponse object into json format.
func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}
