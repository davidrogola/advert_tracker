package transport

import (
	"advert_tracker/http/endpoints"
	"advert_tracker/services"
	"context"
	"encoding/json"
	"net/http"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

// MakeAgencyHTTPHandler ..
func MakeAgencyHTTPHandler(service services.AgencyService, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
	}

	addAgencyHandler := kithttp.NewServer(
		endpoints.MakeAddAgencyEndpoint(service),
		decodeAddAgencyRequest,
		encodeResponse,
		opts...,
	)

	getAgencyHandler := kithttp.NewServer(
		endpoints.MakeGetAgencyByIDEndpoint(service),
		decodeGetAgencyByIDRequest,
		encodeResponse,
		opts...,
	)

	getAllAgenciesHandler := kithttp.NewServer(
		endpoints.MakeGetAllAgenciesEndpoint(service),
		decodeGetAllAgenciesRequest,
		encodeResponse,
		opts...,
	)

	linkClientHandler := kithttp.NewServer(
		endpoints.MakeLinkClientEndpoint(service),
		decodeLinkClientRequest,
		encodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("agency/add", addAgencyHandler).Methods("POST")
	r.Handle("agency/get_by_id", getAgencyHandler).Methods("POST")
	r.Handle("agency/get_all", getAllAgenciesHandler).Methods("POST")
	r.Handle("agency/link_client", linkClientHandler).Methods("POST")

	return r

}

func decodeAddAgencyRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var rq endpoints.AddAgencyRequest
	if err := json.NewDecoder(r.Body).Decode(&rq); err != nil {
		return nil, err
	}
	return rq, nil
}

func decodeGetAgencyByIDRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var rq endpoints.GetAgencyByIDRequest
	if err := json.NewDecoder(r.Body).Decode(&rq); err != nil {
		return nil, err
	}
	return rq, nil
}

func decodeGetAllAgenciesRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var rq endpoints.GetAllAgenciesReq
	if err := json.NewDecoder(r.Body).Decode(&rq); err != nil {
		return nil, err
	}
	return rq, nil
}

func decodeLinkClientRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var rq endpoints.LinkClientRequest
	if err := json.NewDecoder(r.Body).Decode(&rq); err != nil {
		return nil, err
	}
	return rq, nil
}
