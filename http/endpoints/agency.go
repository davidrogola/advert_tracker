package endpoints

import (
	"advert_tracker/services"
	"context"

	"github.com/go-kit/kit/endpoint"
)

// AddAgencyRequest ...
type AddAgencyRequest struct {
	Name          string `json:"name"`
	PhoneNumber   string `json:"phone_number"`
	Email         string `json:"email"`
	Address       string `json:"address"`
	ContactPerson string `json:"contact_person"`
}

type addAgencyResponse struct {
	AgencyID int    `json:"agency_id"`
	Message  string `json:"message"`
	Status   bool   `json:"status"`
	Error    error  `json:"error,omitempty"`
}

// MakeAddAgencyEndpoint returns endpoint for creating an agency
func MakeAddAgencyEndpoint(agencyService services.AgencyService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		addAgencyReq := request.(AddAgencyRequest)
		response, err := agencyService.Add(addAgencyReq.Name, addAgencyReq.PhoneNumber, addAgencyReq.Email,
			addAgencyReq.ContactPerson, addAgencyReq.Address)
		if err != nil {
			return addAgencyResponse{
				Message: "An error occured while adding a new agency",
				Error:   err,
			}, nil
		}

		return addAgencyResponse{
			AgencyID: response,
			Message:  "Succesfully added a new agency",
			Status:   true,
		}, nil
	}
}

// GetAgencyByIDRequest ...
type GetAgencyByIDRequest struct {
	AgencyID int `json:"agency_id"`
}

// GetAgencyResponse ...
type GetAgencyResponse struct {
	AgencyInfo *services.AgencyInfo `json:"agency_info"`
	Message    string               `json:"message"`
	Status     bool                 `json:"status"`
	Error      error                `json:"error,omitempty"`
}

// MakeGetAgencyByIDEndpoint returns agency given Id
func MakeGetAgencyByIDEndpoint(agencyService services.AgencyService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		getAgencyReq := request.(GetAgencyByIDRequest)
		agencyInfo, err := agencyService.GetByID(getAgencyReq.AgencyID)
		if err != nil {
			return GetAgencyResponse{
				Message: "An error occured while fetching agency details",
				Error:   err,
			}, nil
		}

		return GetAgencyResponse{
			AgencyInfo: agencyInfo,
			Status:     true,
			Message:    "Succesfully fetched agency details",
		}, nil
	}
}

// LinkClientRequest ...
type LinkClientRequest struct {
	AgencyID int `json:"agency_id"`
	ClientID int `json:"client_id"`
}

// LinkClientResponse ...
type LinkClientResponse struct {
	Message string `json:"message"`
	Status  bool   `json:"status"`
	Error   error  `json:"error,omitempty"`
}

// MakeLinkClientEndpoint links a client to an agency
func MakeLinkClientEndpoint(agencyService services.AgencyService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		linkReq := request.(LinkClientRequest)
		resp, err := agencyService.LinkClient(linkReq.AgencyID, linkReq.ClientID)
		if err != nil {
			return LinkClientResponse{
				Message: "An error occured while linking an agency to a client",
				Status:  false,
				Error:   err,
			}, nil
		}
		return LinkClientResponse{
			Message: "Successfully linked the client to the agency",
			Status:  resp,
		}, nil
	}
}

// GetAllAgenciesReq ...
type GetAllAgenciesReq struct {
}

// GetAgenciesResponse ...
type GetAgenciesResponse struct {
	Agencies []*services.AgencyInfo
	Message  string `json:"message"`
	Status   bool   `json:"status"`
	Error    error  `json:"error,omitempty"`
}

// MakeGetAllAgenciesEndpoint ...
func MakeGetAllAgenciesEndpoint(agenyService services.AgencyService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		agencies, err := agenyService.GetAll()
		if err != nil {
			return GetAgenciesResponse{
				Message: "An error occured while fetching agencies",
				Error:   err,
			}, nil
		}
		return GetAgenciesResponse{
			Message:  "Successfuly fetched agencies",
			Agencies: agencies,
			Status:   true,
		}, nil
	}
}
