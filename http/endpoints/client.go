package endpoints

import (
	"context"
	"fmt"

	"advert_tracker/services"

	"github.com/go-kit/kit/endpoint"
)

// AddClientRequest ...
type AddClientRequest struct {
	Name          string `json:"name"`
	ClientTypeID  int    `json:"client_type_id"`
	PhoneNumber   string `json:"phone_number"`
	Email         string `json:"email"`
	Address       string `json:"address"`
	ContactPerson string `json:"contact_person"`
}

type addClientResponse struct {
	ClientID int    `json:"client_id"`
	Message  string `json:"message"`
	Status   bool   `json:"status"`
	Error    error  `json:"error,omitempty"`
}

// MakeAddClientEndpoint ...
func MakeAddClientEndpoint(s services.ClientService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddClientRequest)
		id, err := s.AddClient(req.ClientTypeID, req.Name, req.PhoneNumber, req.Email, req.ContactPerson, req.Address)
		if err != nil {
			return addClientResponse{
				ClientID: id,
				Message:  "An error occured while adding a new client",
				Error:    err,
			}, nil
		}
		return addClientResponse{
			ClientID: id,
			Message:  "New client added sucessfully",
			Status:   true,
			Error:    err,
		}, nil
	}
}

// GetClientRequest ...
type GetClientRequest struct {
	ClientID int `json:"client_id"`
}

type getClientResponse struct {
	Client  *services.ClientInfo `json:"client_info"`
	Message string               `json:"message"`
	Status  bool                 `json:"status"`
	Error   error                `json:"error,omitempty"`
}

// MakeGetClientByIDEndpoint ...
func MakeGetClientByIDEndpoint(s services.ClientService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetClientRequest)
		client, err := s.GetClientByID(req.ClientID)
		if err != nil {
			return getClientResponse{
				Client:  nil,
				Message: fmt.Sprintf("An error occured while fetching client with Id %d", req.ClientID),
				Error:   err,
			}, nil
		}
		if client == nil {
			return getClientResponse{
				Message: fmt.Sprintf("Client with Id %d not found", req.ClientID),
			}, nil
		}
		return getClientResponse{
			Client:  client,
			Message: "Succesfully fetched client details",
			Status:  true,
		}, nil
	}
}

// GetAllClientsRequest ...
type GetAllClientsRequest struct {
}

// GetAllClientsResponse ...
type GetAllClientsResponse struct {
	Clients []*services.ClientInfo `json:"clients"`
	Message string                 `json:"message"`
	Status  bool                   `json:"status"`
	Error   error                  `json:"error,omitempty"`
}

// MakeGetAllClientsEndpoint ...
func MakeGetAllClientsEndpoint(s services.ClientService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		clients, err := s.GetAllClients()
		if err != nil {
			return GetAllClientsResponse{
				Clients: nil,
				Message: fmt.Sprintf("An error occured while fetching clients"),
				Error:   err,
			}, nil
		}
		if clients == nil {
			return GetAllClientsResponse{
				Message: fmt.Sprintf("Clients not found"),
			}, nil
		}
		return GetAllClientsResponse{
			Clients: clients,
			Message: "Succesfully fetched client details",
			Status:  true,
			
		}, nil
	}
}
